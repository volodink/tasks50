import sys
sys.path.insert(0, "src/")

print(sys.path)

from utils import get_answer
from utils import Counter

def test_counter_set():
    cnt = Counter(10)
    cnt.dec()
    assert cnt.get_value() == 9

def test_counter_inc():
    cnt = Counter()
    cnt.inc()
    assert cnt.get_value() == 1

def test_counter():
    cnt = Counter()
    cnt.inc()
    cnt.dec()
    assert cnt.get_value() == 0

def test_answer():
    assert 42 == get_answer()

def test_answer42():
    assert 42 == 42

